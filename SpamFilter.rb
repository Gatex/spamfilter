#adding library for getting user's input
require 'io/console' 

puts "Welcome to SpamFilter!\n"

#Atrenkamų leksem'ų failo patikrinimo metu skaičius
lexemCount = 20
#Kintamasis, kuriame saugome tikimybę, kad failas yra spam'as
fileProbability = 0.0

neutralProb = 0.6

#Failo, kurį tikrinsime vieta
fileToCheck = './45_ne_spam.txt'

#Apsirašome hash lenteles, kurioje saugosime spam ir ne spam failuose esančius žodžius ir jų kiekius
sWords = Hash.new(0)
nWords = Hash.new(0)

#Apsirašome hash lentelę, kurioje saugosime visus žodžius, su spam ir ne spam santykiais su visais žodžiais
snProbabilities = Hash.new{|key, value| key[value] = []}

#Apsirašome hash lentelę, kurioje saugosime visus žodžius su tikimybėmis, kad žodis yra spam
spamProbabilities = Hash.new(0)

#Apsirašome hash lentelę, kurioje saugosime tikrinamo failo visas lexemas ir jų tikimybes, kad jos yra spam
allFileLexemProbabilities = Hash.new(0)
#Apsirašome hash lentelę, kurioje saugosime tikrinamo failo atitinkamą skaičių lexemų ir jų tikimybes, kad jos yra spam
chosenFileLexemProbabilities = Hash.new(0)

#Apsimokymo failo nuskaitymo metodas su regural expressions panaudojimu
def read_file (var1, var2)
	Dir.glob(var2) do |f|
		file = File.open(f, "r")
		file.each_line do |line|
			words = line.split(" ")
			words.each do |word|
				word = /[a-zA-Z]+/.match(word).to_s.downcase
				word = word.strip
				if  word != nil && !word.empty? then
					var1[word] += 1
				end
			end
		end
		file.close
	end
end

#Metodas, žodžių santykiams su visais žodžiais apskaičiuoti
def countProbabilities (hash, count)
	hash.each do |key, value|
		hash[key] = value/count.to_f
	end
end

#Apsimokymo failų nuskaitymo metodų iškvietimas
read_file(sWords, './Spamas/*.txt')
read_file(nWords, './Nespamas/*.txt')

#Hash lentelėse esančių žodžių skaičiaus nustatymas
sWordCount = sWords.size
nWordCount = nWords.size

#Metodų, žodžių santykiams su visais žodžiais apskaičiuoti, iškvietimas
countProbabilities(sWords, sWordCount)
countProbabilities(nWords, nWordCount)

#Santykių perrašymas į hash lentelę
sWords.each do |key, value|
	snProbabilities[key][0] = value
	snProbabilities[key][1] = 0
end

#Santykių perrašymas į hash lentelę
nWords.each do |key, value|
	snProbabilities[key][1] = value
	if(snProbabilities[key][0] == nil)
		snProbabilities[key][0] = 0
	end
end

#Tikimybių apskaičiavimas
snProbabilities.each do |key, value|
	spamProbabilities[key] = value[0] / (value[0] + value[1])
end

#Metodas, failo tikrinimui dėl spam'o
def scan_file (var1, var2, var3)
	Dir.glob(var1) do |f|
		file = File.open(f, "r")
		file.each_line do |line|
			words = line.split(" ")
			words.each do |word|
				word = /[a-zA-Z0-9]+/.match(word).to_s.downcase
				word = word.strip
				if var2.key?(word) && !word.empty? then
					var3[word] = var2[word]
				else
					var3[word] = 0.4
				end
			end
		end
		file.close
	end
end

#Metodas, leksemų su atitinkamom tikimybėm atrinkimui
def choose_lexems(var, var1, var2, neutralPr)
	lexCount = var2
	varr = var.clone
	var.each do |key, value|
		varr[key] = (neutralPr - value).abs
	end
	varr = varr.sort_by { |key, value| value}.reverse.to_h
	varr.each do |key, value|
		if lexCount > 0 && var[key] != 0 && var[key] != 1 then
			var1[key] = var[key]
			lexCount -= 1
		end
	end
end

#Metodas, failo spam'o tikimybei apskaičiuoti
def count_probability(var, var1)
	temp = var1.values[0]
	temp2 = var1.values[0]
	var1.each do |key, value|
		temp *= (1 - value)
		temp2 *= value
	end
	var = temp2 / (temp + temp2)
	return var
end


def read_files (dirName, spamProb, allFileLexemProb, chosenFileLexemProb, lexemC, fileProb, neutralPr)
	spamFilesCounter = 0
	hamFilesCounter = 0
	Dir.glob(dirName) do |f|
		file = File.open(f, "r")
		chosenFileLexemProb.clear
		allFileLexemProb.clear
		#Metodo, failo tikrinimui, iškvietimas
		scan_file(file, spamProb, allFileLexemProb)

		#Metodo, leksemų su atitinkamomis tikimybėmis atrinkimui, iškvietimas
		choose_lexems(allFileLexemProb, chosenFileLexemProb, lexemC, neutralPr)

		#Informacijos išvedimas
		puts
		puts "Chosen Lexems with highest probability of being in a spam:"
		chosenFileLexemProb.each do |key, value|
			print key
			puts " %.3f" %value
		end

		#Failo spam'o tikimybės priskyrimas kintamajam, iškviečiant metodą
		fileProb = count_probability(fileProb, chosenFileLexemProb)

		#Informacijos išvedimas
		if fileProb > 0.2 then
			puts "Failas %s yra spam'as, tikimybė: %.8f" % [f, fileProb]
			spamFilesCounter += 1
		elsif fileProb < 0.2 then
			puts "Failas %s yra ne spam'as, tikimybė: %.8f" % [f, fileProb]
			hamFilesCounter += 1
		end

		file.close
	end
	puts "Iš viso aptikta spam failų: %i" % spamFilesCounter
	puts "Iš viso aptikta ne spam failų: %i" % hamFilesCounter
	performance = hamFilesCounter/(spamFilesCounter + hamFilesCounter).to_f
	puts "Teisingai atspėta: %f" %performance
end

read_files('./Test/*.txt', spamProbabilities, allFileLexemProbabilities, chosenFileLexemProbabilities, lexemCount, fileProbability, neutralProb)


puts sWordCount
puts nWordCount